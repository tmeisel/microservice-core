package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Environment interface {
	IsHealthy() bool
}

type Server struct {
	router      *gin.Engine
	baseGroup   *gin.RouterGroup
	environment Environment
}

const (
	StatusHealthy   = "healthy"
	StatusUnhealthy = "unhealthy"
)

// New creates default gin server with basic route configuration.
func New(environment Environment) *Server {
	server := &Server{
		environment: environment,
	}

	server.initRoutes()

	return server
}

// Run starts up the server on given port.
func (s *Server) Run(port int) error {
	if err := s.router.Run(fmt.Sprintf(":%d", port)); err != nil {
		return err
	}

	return nil
}

// Add runs given function to add endpoints to gin RouterGroup.
//
// Example of addFunc:
//
//	func addUserRoutes(rg *gin.RouterGroup) {
//		users := rg.Group("/users")
//
//		users.GET("/", func(c *gin.Context) {
//			c.JSON(http.StatusOK, "users")
//		})
//		users.GET("/comments", func(c *gin.Context) {
//			c.JSON(http.StatusOK, "users comments")
//		})
//		users.GET("/pictures", func(c *gin.Context) {
//			c.JSON(http.StatusOK, "users pictures")
//		})
//	}
func (s *Server) Add(addFunc func(*gin.RouterGroup)) {
	addFunc(s.baseGroup)
}

func (s *Server) initRoutes() {
	s.router = gin.Default()

	v1 := s.router.Group("/v1")

	metricsGroup := v1.Group("/metrics")
	metricsGroup.GET("/health", s.healthHandlerFunc)

	baseGroup := v1.
		Group("/tenants").
		Group("/:tenantId")

	s.baseGroup = baseGroup
}

func (s *Server) healthHandlerFunc(c *gin.Context) {
	healthStatus := StatusUnhealthy
	if s.environment.IsHealthy() {
		healthStatus = StatusHealthy
	}

	c.JSON(http.StatusOK, gin.H{
		"status": healthStatus,
	})
}
