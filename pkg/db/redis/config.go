package redis

import "fmt"

type Config struct {
	Host     string
	Port     int
	Username string
	Password string
	Database int
}

func (c Config) DSN() string {
	return fmt.Sprintf("redis://%s:%s@%s:%d/%d", c.Username, c.Password, c.Host, c.Port, c.Database)
}
