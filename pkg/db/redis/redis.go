package redis

import (
	"context"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/sethvargo/go-retry"
)

func ConnectRetry(ctx context.Context, config Config, maxDuration time.Duration, errChan chan<- error) (*redis.Client, error) {
	opt, err := redis.ParseURL(config.DSN())
	if err != nil {
		return nil, fmt.Errorf("could not parse connUrl '%s': %w", config.DSN(), err)
	}

	client := redis.NewClient(opt)

	backoff := retry.NewFibonacci(time.Millisecond * 500)
	backoff = retry.WithCappedDuration(time.Second*30, backoff)
	backoff = retry.WithJitter(time.Millisecond*50, backoff)
	backoff = retry.WithMaxDuration(maxDuration, backoff)

	if err = retry.Do(ctx, backoff, func(ctx context.Context) error {
		if _, err := client.Ping(ctx).Result(); err != nil {
			err = fmt.Errorf("connection check failed: error: %w", err)
			errChan <- err

			return retry.RetryableError(err)
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return client, nil
}
